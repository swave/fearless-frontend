//<div class="card">
function createCard(name, description, pictureUrl, starts, ends, location) {
    const startDate = new Date(starts);
    const endDate = new Date(ends);

    const options = {year: 'numeric', month: '2-digit', day: '2-digit'};
    const startStr = startDate.toLocaleDateString('en-US', options);
    const endStr = endDate.toLocaleDateString('en-US', options);


    return `
    <div class="col-mb-4 mb-4">
      <div class="card shadow-lg mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>


          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${startStr} - ${endStr}
        </div>
      </div>
    </div>
    `;
}
console.log("name")
window.addEventListener('DOMContentLoaded', async () => {
    console.log("this is working");

    const url = 'http://localhost:8000/api/conferences/';

    const cols = document.querySelectorAll('.col');
    let colIndex = 0;

    try{
        const response = await fetch(url);
        //console.log(response);

        if(!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);

        } else{
            const data = await response.json();
            //console.log(data);

            for (let conference of data.conferences) {

                //const conference = data.conferences[0];
                //const nameTag = document.querySelector('.card-title');

                //nameTag.innerHTML = conference.name;
                //console.log(conference);

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {

                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const location = details.conference.location.name;
                    //const description = document.querySelector('.card-text');
                    //const imageTag = document.querySelector('.card-img-top');
                    //imageTag.src = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);

                    //description.innerHTML = details.conference.description;
                    //const column = document.querySelector('.col');
                    //column.innerHTML += html;
                    //console.log(html);
                    //const row = document.querySelector('#conferencesRow');
                    //row.innerHTML += html;

                    cols[colIndex].innerHTML += html;
                    colIndex = (colIndex + 1) % cols.length;
                    }
                }
        }
    } catch (e) {
    console.error(`Fetch failed: ${e.message}`);

    document.body.innerHTML += `
      <div class="alert alert-danger" role="alert">
        Error: ${e.message}
      </div>
     `;




    //console.error(`THis is broken:`)

    }



});
