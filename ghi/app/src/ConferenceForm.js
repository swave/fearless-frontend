import React, { useEffect, useState } from 'react';
import logo from './logo.svg';

function ConferenceForm() {

  const [name, setName] = useState('');
  const [startDate, setStartDate] = useState('');
  const [ endDate, setEndDate] = useState('');
  const [ description, setDescription] = useState('');
  const [ maxPresentations, setMaxPresentations] = useState('');
  const [ maxAttendees, setMaxAttendees] = useState('');
  const [ chooseLocation, setChooseLocation] = useState([]);
  const [ selectChooseLocation, setSelectChooseLocation] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.starts = startDate;
    data.ends = endDate;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees = maxAttendees;
    data.location = selectChooseLocation;

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
          'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

        setName('');
        setStartDate('');
        setEndDate('');
        setDescription('');
        setMaxPresentations('');
        setMaxAttendees('');

        setSelectChooseLocation('');
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setChooseLocation(data.locations);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleStartDate = (event) => {
    const value = event.target.value;
    setStartDate(value);
  };

  const handleEndDate = (event) => {
    const value = event.target.value;
    setEndDate(value);
  };

  const handleDescription = (event) => {
    const value = event.target.value;
    setDescription(value);
  };

  const handleMaxPresentations = (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  };

  const handleMaxAttendees = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  };

  const handleSelectChooseLocation = (event) => {
    const value = event.target.value;
    setSelectChooseLocation(value);
  };

  return (
    <div className="row">
      <img src={logo} className="App-logo" alt="logo" />
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference234234</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartDate} value={startDate} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>
             <div className="form-floating mb-3">
              <input onChange={handleEndDate} value={endDate} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea onChange={handleDescription} value={description} placeholder="Text" rows="4" required type="text" name="description" id="description" className="form-control"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxPresentations} value={maxPresentations} placeholder="Max_presentations" required type="text" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxAttendees} value={maxAttendees} placeholder="Max_attendees" required type="text" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select onChange={handleSelectChooseLocation} required id="location" className="form-select" name="location">
                <option value="">Choose location</option>
                {chooseLocation.map(location => { // Fixed variable name from chooseLocations to chooseLocation
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ConferenceForm;
